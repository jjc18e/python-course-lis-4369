# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 10:56:24 2021

@author: JJ
"""

import demo as d

def main():
    d.get_requirements()
    d.data_analysis_2()

if __name__ == "__main__":
    main()