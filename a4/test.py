# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 10:22:22 2021

@author: JJ
"""

import re 
import numpy as np
np.set_printoptions(threshold=np.inf)
import pandas as pd

url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
df = pd.read_csv(url)

print("***DataFrame composed of three components: index, columns, and data. Data also known as values.***")
index = df.index
columns = df.columns
values = df.values

print("\n1. Print indexes: ")
print(index)

print("\n2. Print columns: ")
print(columns)

print("\n3. Print columns (another way): ")
print(df.columns[:])

print("\n4. Print (all) values, in array format: ")
print(values)

print("\n5. ***Print component data types:***")
print("\na) index type:")
print(type(index))

print("\nb) columns type:")
print(type(columns))

print("\nc) value type:")
print(type(values))

print("\n6. Print summary of DataFrame (similar to 'describe tablename;' ub MYSQL:")
print(df.info())

print("\n7. First five lines (all columns):")
print(df.head())

df = df.drop('Unnamed: 0',1)
print("\n8. Print summary of DataFrame (after dropping column 'Unnamed: 0'):")
print(df.info())

print("\n9. First five lines (after dropping column 'Unamed: 0'):")
print(df.head())

print("\n***Precise data selection (data slicing):***")
print("\n10. Using iloc, returns first 3 rows:")
print(df.iloc[:3])

print("\n11. Using ilocm return last 3 rows (start on index 1310 to end):")
print(df.iloc[1310:])

print("\n12. Select rows 1, 3, and 5; and columns 2, 4, and 6 (includes index column):")
a = df.iloc[[0, 2, 4], [1, 3, 5]]
print(a)

print("\n13. Select all rows; and columns 2, 4, and 6 (includes index column):")
a = df.iloc[:,[1,3,5]]
print(a)

print("\n14. Select rows 1, 3, and 5; and all columns (includes index column):")
a = df.iloc[[0, 2, 4],:]
print(a)

print("\n15. Select all rows, and  all cilumns (includes index column) Note: only first and last 30 records displayed:")
a = df.iloc[:,:]
print(a)

print("\n16. Select all rows, and columns, starting at column 2 (includes index column) Note: only first and last 30 records displayed:")
a = df.iloc[:,1:]
print(a)

print("\n17. Select row 1, and column 1, (includes index column):")
a = df.iloc[0:1, 0:1]
print(a)

print("\n18. Select rows 3-5, and columns 3-5, (includes index column)")
a = df.iloc[2:5, 2:5]
print(a)


print("\n19. ***Convert pandas DataFrame df to NumPy ndarray, use values command:***")
b = df.iloc[:,1:].values

print("\n20. Print data frame type:")
print(type(df))

print("\n21. Print a type:")
print(type(a))

print("\n22. Print b type:")
print(type(b))

print("\n23. Print number of dimensions and items in array (rows, columns). Remember: starting at column 2:")
print(b.shape)

print("\n24. Print type of items in array. Remember: ndarray is an array of arrays. Each record/item is an array.")
print(b.dtype)

def data_analysis_2():
    print("\nGraph: Display the first 20 passengers: ")
    c = df['Age'].head(20)
    c.plot()
    plt.xticks(np.arange(0, 20, 1.0))
    plt.show()

get_requirements()
data_analysis_2()



