# LIS 4369

## John Corrigan

### Assignment #4 Requirements:

*Sub-Heading:*

1. Run demo.py
2. Fix any errors
3. Must use 3 functions

#### README.md file should include the following items:

* Screenshot of data_anlysis_2 results
* Screenshot of jupyter notebook
* Skillsets Screenshots

#### Assignment Screenshots:

*Screenshot of data_analysis_2 http://localhost*:

![Data Analysis Screenshot](img/data_analysis_2.png)

*Screenshot of Jupyter Notebook*:

![Jupyter notebook Screenshot](img/jupyter_1.png)
![Jupyter notebook Screenshot](img/jupyter_2.png)
![Jupyter notebook Screenshot](img/jupyter_3.png)

*Screenshot of Skillset 10*:

![Skillset 10 Screenshot](img/ss10_screenshot.png)

*Screenshot of Skillset 11*:

![Skillset 11 Screenshot](img/ss11_screenshot.png)

*Screenshot of Skillset 12*:

![Skillset 12 Screenshot](img/ss12_screenshot.png)
