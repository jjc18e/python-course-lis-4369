> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4369

## John Corrigan

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1 tip calculator* Jupyter Notebook
    - Provide Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2.  [A2 README.md](a2/README.md "My A2 README.md file")
    - Create *a2_payroll_calculator* application
    - Must use seperation of concerns
    - Create *a2 payroll calculator* Jupyter Notebook
    - Test code with both IDLE and your chosen text editor
3.  [A3 README.md](a3/README.md "My A3 README.md file")
    - Create *a3_painting_estimator* application
    - Must use seperation of concerns
    - Create *a3 painting estimator* Jupyter Notebook
    - Test code with both IDLE and your chosen text editor
4.  [A4 README.md](a4/README.md "My A4 README.md file")
    - Create *data_analysis_2* application
    - Must use seperation of concerns
    - Create *data analysis 2* Jupyter Notebook
    - Test code with both idle and chosen text editor
5.  [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete R tutorial and code learn_to_use_r.R
    - Create and Screenshot 2 graphs
    - Code lis4369_a5.R
    - Create and Screenshot 2 graphs
6.  [P1 README.md](p1/README.md "My P1 README.md file")
    - Create *data_analysis_1* application
    - Must use seperation of concerns
    - Create *data analysis 1* Jupyter Notebook
    - Test code with both IDLE and chosen text editor
7.  [P2 README.md](p2/README.md "My P2 README.md file")
    - Analyze mtcars data by following requirements
    - create and screenshot 2 plots
    - Test and run code with Rstudio
