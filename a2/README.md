

# Course LIS4369

### John Corrigan

### Assignment #2 Requirements:

*3 parts:*

1. Create payroll calculator application
2. Create payroll calculator jupyter notebook
3. screenshots of payroll calculator with and without overtime

#### README.md file should include the following items:

* assignment requirements per A2
* screenshots of payroll calculator with and without overtime
* link to ipynb file


#### Assignment Screenshots:

#### Screenshot Payroll No Ovetime:

![Payroll No Overtime Screenshot](img/payroll_no_overtime.png)

#### Screenshot Payroll with Overtime:

![Payroll with Overtime Screenshot](img/payroll_overtime.png)

#### A2 Jupyter Notebook:

![Jupyter Notebook ipynb file](img/jupyter_notebook_p1.png)
![Jupyter Notebook ipynb file](img/jupyter_notebook_p2.png)
#### Skillset 1 Screenshot

![Skillset 1 Screenshot](img/ss1_screenshot.png)

#### Skillset 2 Screenshot

![Skillset 2 Screenshot](img/ss2_screenshot.png)

#### Skillset 3 Screenshot

![Skillset 3 Screenshot](img/ss3_screenshot.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jjc18e/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jjc18e/myteamquotes/ "My Team Quotes Tutorial")
