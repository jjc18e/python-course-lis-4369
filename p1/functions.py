# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 20:46:38 2021

@author: JJ
"""

def get_requirements():
    print("Data Analysis 1\n")
    print("Developed by: John Corrigan\n")
    print("Program Requirements:\n"
          + "1. Run demo.py.\n"
          + "2. If errors, more than likely missing installations.\n"
          + "3. Test Python Package Installer: pip freeze\n"
          + "4. Research how to do the follwing installations:\n"
              + "\ta. Pandas (only if missing\n"
              + "\tb. pandas-datareader (only if missing)\n"
              + "\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program:\n"
          + "\ta. main(): calls at least the other two functions.\n"
          + "\tb. get_requirements(): displays the program requirements.\n"
          + "\tc. data_analysis_1(): displays the following data.")
    
def data_analysis_1():
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as pit
    from matplotlib import style
    
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()
    
    df = pdr.DataReader(["DJIA", "SP500"], "fred", start, end)
   
    print("\nPrint number of records:")
    print(df.size)
     
   
    print("\nPrint columns:")
    print(df.columns)
    
    print("\nPrint data frame:")
    print(df)
    
    print("\nPrint first five lines:")
    print(df[:5])
    
    print("\nPrint last five lines:")
    print(df[-5:])
    
    print("\nPrint first 2 lines:")
    print(df[:2])
    
    print("\nPrint last 2 lines:")
    print(df[-2:])
    
    style.use('ggplot')

    df['DJIA'].plot()
    df['SP500'].plot()
    pit.legend()
    pit.show()

get_requirements()
data_analysis_1()
