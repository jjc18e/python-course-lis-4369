# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 10:53:40 2021

@author: JJ
"""

import functions as f

def main():
    f.get_requirements()
    f.data_analysis_1()
    
if __name__ == "__main__":
    main()
