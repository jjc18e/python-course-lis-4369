# LIS 4369

## John Corrigan

### Assignment # Requirements:

*Sub-Heading:*

1. Backward-Engineer using python
2. The program should be two modules functions.py and main.py
3. main() must call at least two other functions

#### README.md file should include the following items:

* Project requiremnets as per P1
* screenshot of reverse engineered program
* Screenshot of produced graph

#### Assignment Screenshots:

*Screenshot of Data analysis 1 http://localhost*:

![Data Analysis Screenshot](img/data_analysis_1.png)

*Screenshot of Graph*:

![JDK Installation Screenshot](img/stock_chart.png)

*Screenshot of Jupyter Notebook*:

![Jupyter Notebook](img/jupyter_notebook1.png)
![Jupyter Notebook](img/jupyter_notebook2.png)

SkillSet 7 Screenshot*:

![Skillset 7 Screenshot](img/ss7_screenshot.png)

*Skillset 8 Screenshot*:

![Skillset 8 Screenshot](img/ss8_screenshot.png)

*Skillset 9 Screenshot*:

![Skillset 9 Screenshot](img/ss9_screenshot.png)