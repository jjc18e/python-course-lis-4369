import math as m

def get_requirements():
    print("Sphere Volume Program")
    print("Developed by: John Corrigan")

    print("\nProgram Requirements:")
    print("Program calculates sphere volime in liquid U.S. gallons from user entered diameter in value in inches, and rounds to two decimal places.")
    print("2. Must use Python's *built-in* PI and pow() capabilities.")
    print("3. Program checks for non-intergers and non-numeric values.")
    print("4. Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letter.\n")

def calculate():
    pi = 3.14159265;
    di = 0.0
    frac = 1.33333;
    gal = 19.25317;
    choice = ' '
    ans = 0;

    print("Input: ")
    choice = input("Do you want to calculate a sphere volume (y or n)?").lower()

    print("Output: ")

    while(choice == 'y'):
        while True:
            try:
                dia = int(input("\nPlease enter diameter in inches (intergers only): "))
                dia = dia*.5

                ans = (((m.pow(dia,3))*frac*pi)/12)/gal
                ans = str(round(ans, 2))
                print("\nSphere volume: " + ans + " liquid U.S. gallons.")

                choice = input("Do you want to calculate a sphere volume (y or n)? ")

            except ValueError:
                print("Not an interger! Try again.")
                continue
            else:
                break
    else:
        print("\nThanks for using our Calculator!")

