import math as m

def get_requirements():
    print("Python Calculator w/ Error Handling")
    print("Developed by: John Corrigan\n")
    print("Program Requriements: \n"
        + "1. Calculates two numbers and rounds to two decimal places.\n"
        + "2. Prompts user for two numbers and a suitable operator.\n"
        + "3. Uses Python error handling to validate data.\n"
        + "4. Tests for correct arithmetic operator.\n"
        + "5. Division by zero is not permitted.\n"
        + "6. Program loops untul correct input is entered.")

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a number! Try again!")

def getOp():
    validOperators = ['+','-','*','/','//','%','**']
    while True:
        op = input("\nSuitable Operators: +, -, *, /, // (interger division), % (modulo operator), ** (power): ")
        try:
            validOperators.index(op)
            return op
        except ValueError:
            print("Invalid operator! Try again!")

def calc():
    num1 = getNum("Enter num1:")
    num2 = getNum("Enter num2:")
    op = getOp()
    sum = 0.00

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sun = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by Zero! Re-enter num2.")
    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2.")
    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2.")
    
    print("\nAnswer is " + str(round(sum,2)))
    print("\nThank you for using our Math Calculator!")