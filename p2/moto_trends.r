rm(list = ls(envir = globalenv()), envir = globalenv()); if(!is.null(dev.list())) dev.off(); gc(); cat("\014")

setwd('A:/repos/lis4369/p2/')

url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
mtcars <- read.csv(file=url,head=TRUE,sep=",")

mtcars

head(mtcars,10)

tail(mtcars,10)

str(mtcars)

names(mtcars)

mtcars[1,]

mtcars[,2]

mtcars[,'cyl']

mtcars[3,4]

mtcars[mtcars$cyl > 4,]

mtcars[ which(mtcars$cyl>4 
              & mtcars$gear >= 5), ]

mtcars[ which(mtcars$cyl>4 
              & mtcars$gear == 4), ]

mtcars[ which(mtcars$cyl>4 
              | mtcars$gear == 4), ]


mtcars[ which(mtcars$cyl>4
              & mtcars$gear !=4), ]

nrow(mtcars)

ncol(mtcars)

dim(mtcars)

str(mtcars)

mean(mtcars$hp)

median(mtcars$hp)

min(mtcars$hp)

max(mtcars$hp)

quantile(mtcars$hp)

var(mtcars$hp)

sd(mtcars$hp)

summary(mtcars)