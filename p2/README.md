# LIS 4369

## John Corrigan

### Project #2 Requirements:

*Sub-Heading:*

1. Use Assignment 5 and R manual to backward engineer
2. Use motor trend Car Road tests data
3. Test code in R studio

#### README.md file should include the following items:

* Screenshots of 4 panel R studio
* Screenshots of qplot() and plot()
* no skillsets

#### Assignment Screenshots:

*Screenshot of 4 panel R studio http://localhost*:

![4 Panel R Studio](img/4_panel_screenshot.png)

*Screenshot of qplot*:

![qplot() Screenshot](img/qplot.png)

*Screenshot of Plot*:

![plot() Screenshot](img/plot.png)
