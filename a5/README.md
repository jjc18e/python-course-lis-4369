# LIS 4369

## John Corriegan

### Assignment #5 Requirements:

*Sub-Heading:*

1. Complete the R tutorial 
2. Code, run, and combine both demo.R files
3. save as lis4369_a5.R

#### README.md file should include the following items:

* Screenshots of R tutorial and a5
* 4 chart screenshots 2 from tutrial and 2 from a5
* Skillset Screenshots

#### Assignment Screenshots:

*Screenshot of learn_to_use_r.R http://localhost*:

![R tutorial Screenshot](img/tutorial_screenshot.png)

*Screenshot of tutorial_chart_1*:

![Apple Chart Screenshot](img/tutorial_chart_1.png)

*Screenshot of tutorial_chart_2*:

![Amazon Chart Screenshot](img/tutorial_chart_2.png)

*Screenshot of lis4369_a5_screenshot*:

![LIS 4369 A5 Screenshot](img/lis4369_a5_screenshot.png)

*Screenshot of a5_chart1*:

![Titanic Chart 1 Screenshot](img/a5_chart1.png)

*Screenshot of a5_chart2*:

![Titanic Chart 2 Screenshot](img/a5_chart2.png)

*Screenshot of Skillset 13*:

![Skillset 13 Screenshot](img/ss13_screenshot.png)

*Screenshot of Skillset 14*:

![Skillset 14 Screenshot](img/ss14_screenshot.png)

*Screenshot of Skillset 15*:

![Skillset 15 Screenshot](img/ss15_screenshot.png)

