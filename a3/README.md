# LIS 4369

## John Corrigan

### Assignment #3 Requirements:

*Sub-Heading:*

1. Backward-Engineer using pyhon
2. The program should be two modules functions.py and main.py
3. Test program using IDLE and Visual Studio Code

#### README.md file should include the following items:

* Assignment requirements as per A3
* Screenshot of reverse engineered program
* Upload A3 .ipynb file and create link in read me

#### Assignment Screenshots:

*Screenshot of Paint Estimator http://localhost*:

![Paint Estimator Screenshot](img/painting_estimator_screenshot.png)

*Screenshot of Jupyter Notebook*:

![Jupyter Notebook](img/jupyter_nb1.png)
![Jupyter Notebook](img/jupyter_nb2.png)

*SkillSet 4 Screenshot*:

![Skillset 4 Screenshot](img/ss4_screenshot.png)

*Skillset 5 Screenshot*:

![Skillset 5 Screenshot](img/ss5_screenshot.png)

*Skillset 6 Screenshot*:

![Skillset 6 Screenshot](img/ss6_screenshot.png)
