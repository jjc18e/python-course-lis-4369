> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4369

## John Corrigan

### Assignment #1 Requirements:

*four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Questions
4. Bitbuccket repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1.ipynb file: tip_calculator.ipynb
* git commands w/short descriptions
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - turns director into empty Git repository
2. git status - returns current state of the repository
3. git add -adds files in the staging area for git
4. git commit - records the changes made to a file
5. git push - sends local commits to the remote repository
6. git pull - gets the latest verion of a repository
7. git config - assign setting to git

#### Assignment Screenshots:

#### Screenshot of a1_tip_calculator application running (IDLE):

![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.png "A1 IDLE Screenshot")

#### Screenshot of a1_tip_calculator application running (Visual Studio Code):

![Python Installation Screenshot VS Code](img/a1_tip_calculator_vs_code.png "A1 VS Vode Screenshot")

#### A1 Jupyter Notebook:

![tip_calculator.ipynb](img/a1_jupyter_notebook.png "A1 Jupyter Notebook")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
